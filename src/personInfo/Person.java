package personInfo;

public class Person {
	private String name;
	private String gender;
	private String nationality;
	String[] hobbies;
	boolean isBlueDevil;
	
	public Person(String name, String gender, String nationality, String[] hobbies) {
        this.name = name;
        this.gender = gender;
        this.nationality = nationality;
        this.hobbies = hobbies;
    }
	
	/*
	public boolean is(String name) {
		return this.firstName == firstName && this.lastName == lastName;
	}
	*/
	
	// the output does not contain a newline character.
	protected void describeBasics() {
		System.out.print(name + " is from " + nationality + ".");
	}
	
	protected void printTitle() {
		if (gender == "male") {
			System.out.print("He ");
		} else if (gender == "female") {
			System.out.print("She ");
		}
	}
	
	// the output does not contain a newline character
	protected void describeHobbies() {
		if (hobbies == null || hobbies.length == 0) {
			return;
		}
		printTitle();
		System.out.print("likes ");
		
		if (hobbies.length == 1) {
			System.out.print(hobbies[0] + '.');
			return;
		} 
		for (int i = 0; i < hobbies.length; i++) {
			System.out.print(hobbies[i]);
			if (i == hobbies.length - 2) {
				System.out.print(" and ");
			} else if (i == hobbies.length - 1) {
				System.out.print(".");
			} else {
				System.out.print(", ");
			}
		}
		
	}
	
	public void describe() {
		describeBasics();
		System.out.print(" ");
		describeHobbies();
		System.out.println();
	}
}

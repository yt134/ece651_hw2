package personInfo;

public class BlueDevil extends Person {
	private String DukeStatus;
	private String graduateFrom;
	
	public BlueDevil(String name, String gender, String nationality, String[] hobbies, String DukeStatus, String graduateFrom) {
		super(name, gender, nationality, hobbies);
		this.DukeStatus = DukeStatus;
		this.graduateFrom = graduateFrom;
	}
	
	private void describeStatus() {
		if (DukeStatus == null || DukeStatus == "") {
			return;
		}
		printTitle();
		System.out.print("is a/an " + DukeStatus + ".");
	}
	
	private void describeUndergradSchool() {
		if (graduateFrom == null || graduateFrom == "") {
			return;
		}
		printTitle();
		System.out.print("graduates from " + graduateFrom + ".");
	}
	
	public void describe() {
		describeBasics();
		System.out.print(" ");
		describeStatus();
		System.out.print(" ");
		describeUndergradSchool();
		System.out.print(" ");
		describeHobbies();
		System.out.println();
	}
}

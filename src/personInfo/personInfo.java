package personInfo;
import java.util.*;


public class personInfo {
	public static void whoIs(String name, Map<String, Person> map) {
		if (!map.containsKey(name)) {
			System.out.println("We don't know.");
		} else {
			Person person = map.get(name);
			person.describe();
		}
	}
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Map<String, Person> map = new HashMap<String, Person>();
		map.put("Adel Fahmy", new BlueDevil("Adel Fahmy", "male", "Egypt", new String[]{"tennis", "biking", "gardening", "cooking"}, "Ajunct Assistant Professor", "NCSU"));
		map.put("Yuanyuan Yu", new BlueDevil("Yuanyuan Yu", "female", "China", new String[]{"baseball", "fencing"}, "Meng ECE student", "ECUST"));
		map.put("You Lyu", new BlueDevil("You Lyu", "female", "China", new String[]{"traveling", "music", "history"}, "ECE Master student", ""));
		map.put("Zhongyu Li", new BlueDevil("Zhongyu Li", "male", "China", new String[]{"basketball", "NBA"}, "ECE Master student", "SouthEast University"));
		map.put("Lei Chen", new BlueDevil("Lei Chen", "female", "China", new String[]{"climbing", "animals"}, "ECE Master student", "KAIST"));
		map.put("Shalin Shah", new BlueDevil("Shalin Shah", "female", "India", new String[]{"bodybuilding", "dancing"}, "PhD ECE candidate", "DA-IICT"));
		map.put("Yue Tu", new BlueDevil("Yue Tu", "male", "China", new String[]{"playing basketball", "programming", "workout"}, "ECE Master student", "Nanjing University"));
		map.put("Jianjun Tu", new Person("Jianjun Tu", "male", "China", new String[]{"playing ping-pong", "history", "workout"}));
		whoIs("Yue Tu", map);
		whoIs("Adel Fahmy", map);
		whoIs("You Lyu", map);
		whoIs("Zhongyu Li", map);
		whoIs("Lei Chen", map);
		whoIs("Shalin Shah", map);
		whoIs("Jianjun Tu", map);
	}

}
